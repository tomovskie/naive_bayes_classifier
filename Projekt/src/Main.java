import org.apache.commons.csv.CSVRecord;
import org.omg.CORBA.INTERNAL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {

        //Reading ucna
        List<CSVRecord> records = CSVReader.readFromCSV("car_ucna.csv");
        //Reading testna
        List<CSVRecord> recordsTest = CSVReader.readFromCSVTest("car_testna.csv");
        // Attributes value
        List<CSVRecord> atributeValue = getAttributeValue(records);
        //Types of attribues
        List<String> atributes = getAttributes(records);
        //All attributes values for test
        List<CSVRecord> atributesValueTest = getAttributeValue(recordsTest);
        //Name of classes
        List<String> differents = getNameOfClasses(atributeValue);

        Map<String, Integer> numberOfClassesLearn = getClasses(differents, atributeValue);

        Map<String, Integer> numberOfClassesTest = getClasses(differents, atributesValueTest);
        //Table for each attribute
        List<Object> allTables = createFrequencyTables(atributes, atributeValue, differents);
        //Bayes results
        String rezultati[] = getResultasNaiveBayesAlgorithm(differents, atributesValueTest, numberOfClassesLearn, atributes, allTables);
        //Creating confusion matrix
        String confusionMatrix[][] = createConfusionMatrix(differents);
        //Output matix with end results for recall,precision,F-mera,class
        String results[][] = createResultsMatric(differents, confusionMatrix);

        int conterTrue = 0;
        int counterFalse = 0;
        for (int i = 0; i < rezultati.length; i++) {
            String primerok = atributesValueTest.get(i).get(atributesValueTest.get(i).size() - 1);
            if (primerok.equals(rezultati[i])) {
                for (int c = 0; c < confusionMatrix[0].length - 1; c++) {
                    if (confusionMatrix[0][c].equals(primerok)) {
                        double num = Double.parseDouble(confusionMatrix[c + 1][c]);
                        num++;
                        confusionMatrix[c + 1][c] = String.valueOf(num);
                    }
                }
            } else {
                for (int c = 0; c < confusionMatrix[0].length - 1; c++) {
                    if (confusionMatrix[0][c].equals(rezultati[i])) {
                        for (int j = 0; j < confusionMatrix[0].length - 1; j++) {
                            if (primerok.equals(confusionMatrix[0][j])) {
                                double num = Double.parseDouble(confusionMatrix[j + 1][c]);
                                num++;
                                confusionMatrix[j + 1][c] = String.valueOf(num);
                            }
                        }
                    }
                }
            }
        }

        //Calculating end results for fmeasure,precision,recall and average
        calculatingResults(confusionMatrix, results, numberOfClassesTest);
        //Output end resultas
        outputResults(differents, confusionMatrix, results);

    }

    //How many times each class is repeated
    public static Map<String, Integer> getClasses(List<String> differents, List<CSVRecord> atributeValue) {
        Map<String, Integer> numOfClasses = new HashMap<String, Integer>();
        int totalNumOfClasses = 0;
        double probabilityOfClasses, entropy = 0.0;
        for (int i = 0; i < differents.size(); i++) {
            numOfClasses.put(differents.get(i), 0);
            for (int j = 0; j < atributeValue.size(); j++) {
                if (differents.get(i).equals(atributeValue.get(j).get(atributeValue.get(j).size() - 1))) {
                    int counter = numOfClasses.get(differents.get(i)) + 1;
                    numOfClasses.put(differents.get(i), counter);
                }
            }
            totalNumOfClasses += numOfClasses.get(differents.get(i));
        }
        numOfClasses.put("TotalNumber", totalNumOfClasses);
        return numOfClasses;
    }

    //Value for parameter
    public static List<CSVRecord> getAttributeValue(List<CSVRecord> records) {
        List<CSVRecord> atributeValue = new ArrayList<>();
        for (int i = 1; i < records.size(); ++i) {
            atributeValue.add(records.get(i));
        }
        return atributeValue;
    }

    //Types of attributes
    public static List<String> getAttributes(List<CSVRecord> records) {
        List<String> atributes = new ArrayList<>();
        for (int i = 0; i < records.get(0).size(); i++) {
            atributes.add(records.get(0).get(i));
        }
        return atributes;
    }

    //Types of classes
    public static List<String> getNameOfClasses(List<CSVRecord> atributeValue) {
        List<String> differents = new ArrayList<>(); //Se zacuvuva imeto na klasata
        for (int i = 0; i < atributeValue.size(); i++) {
            if (!differents.contains(atributeValue.get(i).get(atributeValue.get(i).size() - 1))) {
                differents.add(atributeValue.get(i).get(atributeValue.get(i).size() - 1));
            }
        }
        return differents;
    }

    //Calculating number of classes
    public static int getNumberOfClasses(List<String> differents) {
        int numClasses = 0;
        if (differents.size() == 1) {
            numClasses = 0;
        } else {
            numClasses = differents.size();
        }
        return numClasses;
    }

    public static List<String> getDifferentValues(List<CSVRecord> atributeValue,int index){
        List<String> differentValue = new ArrayList<>();
        for (int k = 0; k < atributeValue.size(); k++)
            if (!differentValue.contains(atributeValue.get(k).get(index))) {
                differentValue.add(atributeValue.get(k).get(index));
            }
            return differentValue;
    }

    public static int countDifferentTypeOfValue(List<CSVRecord> atributeValue,int index) {
        List<String> differentValue = new ArrayList<>();
        int numOfRow = 0;
        for (int k = 0; k < atributeValue.size(); k++)
            if (!differentValue.contains(atributeValue.get(k).get(index))) {
                differentValue.add(atributeValue.get(k).get(index));
                numOfRow++;
            }
        return numOfRow;
    }

    public static String[][] creatingTable(List<CSVRecord> atributeValue, List<String> atributes, List<String> differents, int index) {
        int numOfRow = countDifferentTypeOfValue(atributeValue,index);
        String table[][] = new String[numOfRow + 1][getNumberOfClasses(differents) + 2];
        table[0][0] = atributes.get(index);
        table[0][table[0].length - 1] = "All";

        for (int l = 0; l < differents.size(); l++) {
            table[0][l + 1] = differents.get(l);
        }
        for (int l = 1; l < table.length; l++) {
            for (int k = 1; k < getNumberOfClasses(differents) + 2; k++) {
                table[l][k] = "0";
            }
        }
        return table;
    }

    public static String[][] inputValueInTable(List<String>atributes,List<CSVRecord>atributeValue,List<String>differents,int index) {
        String table[][] = creatingTable(atributeValue, atributes, differents, index);
        List<String> differentValue=getDifferentValues(atributeValue,index);
        for (int l = 0; l <differentValue.size(); l++) {
            int sum = 0;
            table[l + 1][0] = differentValue.get(l);
            for (int t = 0; t < atributeValue.size(); t++) {
                if (atributeValue.get(t).get(index).equals(differentValue.get(l))) {
                    sum = Integer.parseInt(table[l + 1][table[0].length - 1]);
                    sum++;
                    table[l + 1][table[0].length - 1] = String.valueOf(sum);
                    int sum1 = 0;
                    for (int d = 0; d < differents.size(); d++) {
                        if (atributeValue.get(t).get(atributeValue.get(index).size() - 1).equals(differents.get(d))) {
                            sum1 = Integer.parseInt(table[l + 1][d + 1]);
                            sum1++;
                            table[l + 1][d + 1] = String.valueOf(sum1);
                        } else {
                            sum1 = Integer.parseInt(table[l + 1][d + 2]);
                            sum1++;
                            table[l + 1][d + 2] = String.valueOf(table[l + 1][d + 2]);
                        }
                    }
                }
            }
        }
        return table;
    }

    //Table for eaech attribute
    public static List<Object> createFrequencyTables(List<String> atributes, List<CSVRecord> atributeValue, List<String> differents) {
        List<Object> allTables = new ArrayList<>();
        for (int i = 0; i < atributes.size(); i++) {
            String table[][]=inputValueInTable(atributes,atributeValue,differents,i);
            allTables.add(table);
        }
        return allTables;
    }

    //Naive Bayes
    public static String[] getResultasNaiveBayesAlgorithm(List<String> differents, List<CSVRecord> atributesValueTest, Map<String, Integer> numberOfClassesLearn, List<String> atributes, List<Object> allTables) {
        String rezultati[] = new String[atributesValueTest.size()];
        for (int i = 0; i < atributesValueTest.size(); i++) {

            CSVRecord row = atributesValueTest.get(i);

            Map<String, Double> forClasses = new HashMap<String, Double>();
            for (String key : numberOfClassesLearn.keySet()
            ) {
                forClasses.put(key, Double.valueOf(numberOfClassesLearn.get(key)));
            }

            for (int j = 0; j < atributes.size() - 1; j++) {

                String table[][] = (String[][]) allTables.get(j);

                for (int a = 1; a < table.length; a++) {
                    if (table[a][0].equals(row.get(j))) {
                        for (int n = 0; n < getNumberOfClasses(differents); n++) {
                            double probab = forClasses.get(table[0][n + 1]) * 1.0 / forClasses.get("TotalNumber");
                            double probabreverse = forClasses.get("TotalNumber") * 1.0 / numberOfClassesLearn.get(table[0][n + 1]);
                            int forClass = Integer.parseInt(table[a][n + 1]);
                            int maxTable = Integer.parseInt(table[a][table[0].length - 1]);
                            double result = 0;
                            result = probab * 1.0 * (forClass * 1.0 / maxTable * 1.0 * probabreverse);
                            forClasses.put(table[0][n + 1], result);
                        }
                    }
                }
            }

            forClasses.remove("TotalNumber");

            Map.Entry<String, Double> maxEntry = null;
            String maxKey = null;
            for (Map.Entry<String, Double> entry : forClasses.entrySet()
            ) {
                if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                    maxEntry = entry;
                    maxKey = maxEntry.getKey();
                }
            }
            rezultati[i] = maxKey;
        }
        return rezultati;
    }

    //Create confusion matrix
    public static String[][] createConfusionMatrix(List<String> differents) {
        String confusionMatrix[][] = new String[getNumberOfClasses(differents) + 1][getNumberOfClasses(differents) + 1];
        confusionMatrix[0][confusionMatrix[0].length - 1] = "<- classified as";
        for (int i = 0; i < confusionMatrix.length - 1; i++) {
            confusionMatrix[i + 1][confusionMatrix[0].length - 1] = differents.get(i);
        }
        for (int i = 0; i < confusionMatrix.length; i++) {
            for (int j = 0; j < differents.size(); j++) {
                if (i == 0)
                    confusionMatrix[i][j] = differents.get(j);
                else
                    confusionMatrix[i][j] = "0";
            }
        }
        return confusionMatrix;
    }

    //Output results
    public static void outputResults(List<String> differents, String[][] confusionMatrix, String[][] results) {
        String format = "";
        for (int i = 0; i < getNumberOfClasses(differents) + 1; i++) {
            format += "%-15s";
        }
        for (int i = 0; i < getNumberOfClasses(differents) + 1; i++) {
            System.out.format(format + "\n", confusionMatrix[i]);
        }

        System.out.println("Accuracy = " + calculateAccuracy(confusionMatrix));

        for (int i = 0; i < results.length; i++) {
            System.out.format("%-20s%-20s%-20s%-20s\n", results[i]);
        }
    }

    //Accuracy calculation
    public static double calculateAccuracy(String[][] confusionMatrix) {
        int correct = 0, incorrect = 0;
        for (int i = 1; i < confusionMatrix.length; i++) {
            for (int j = 0; j < confusionMatrix[i].length - 1; j++) {
                if ((i - 1) == j) {
                    correct += Double.parseDouble(confusionMatrix[i][j]);
                } else {
                    incorrect += Double.parseDouble(confusionMatrix[i][j]);
                }
            }
        }
        double accuracy = correct * 1.0 / (correct + incorrect);
        return accuracy;
    }

    public static void calculatingResults(String confusionMatrix[][], String[][] results, Map<String, Integer> numberOfClassesTest) {
        calculateingFoeEachClass(confusionMatrix, results);
        for (int i = 0; i < results[0].length - 1; i++) {
            double sampNum = 0;
            double rezEnd = 0;
            for (int j = 0; j < results.length - 2; j++) {
                sampNum = numberOfClassesTest.get(results[j + 1][results[0].length - 1]);
                double forClass = Double.parseDouble(results[j + 1][i]);
                double calculated = sampNum * 1.0 / numberOfClassesTest.get("TotalNumber") * forClass;
                rezEnd += calculated;
            }
            results[results.length - 1][i] = String.valueOf(rezEnd);
        }
    }

    //Calculate F-measute,precision,recall for each class
    public static void calculateingFoeEachClass(String confusionMatrix[][], String results[][]) {
        for (int i = 0; i < confusionMatrix.length - 1; i++) {
            double sumFP, sumFN = 0;
            double TP = 0;
            double FN = 0;
            double FP = 0;
            for (int j = 0; j < confusionMatrix[0].length - 1; j++) {
                if (i == j) {
                    TP = Double.parseDouble(confusionMatrix[i + 1][j]);
                } else if (i != j) {
                    sumFP = Double.parseDouble(confusionMatrix[i + 1][j]);
                    FP += sumFP;
                    sumFN = Double.parseDouble(confusionMatrix[j + 1][i]);
                    FN += sumFN;
                }
            }
            double recall = TP * 1.0 / (TP + FN);
            double precision = TP * 1.0 / (TP + FP);
            double fMeasure = 2 * 1.0 * (recall * precision * 1.0 / (recall + precision));

            results[i + 1][0] = String.valueOf(recall);
            results[i + 1][1] = String.valueOf(precision);
            results[i + 1][2] = String.valueOf(fMeasure);

        }
    }

    public static String[][] createResultsMatric(List<String> differents, String confusionMatrix[][]) {
        String results[][] = new String[getNumberOfClasses(differents) + 2][4];
        String firstRow[] = {"recall", "precision", "F-mera", "class"};
        for (int i = 0; i < 4; i++) {
            results[0][i] = firstRow[i];
        }
        for (int i = 1; i < results.length; i++) {
            for (int j = 0; j < 4; j++) {
                if (j == 3 && i != results.length - 1) {
                    results[i][j] = confusionMatrix[0][i - 1];
                } else if (j == 3 && i == results.length - 1) {
                    results[i][j] = "<- average";
                } else {
                    results[i][j] = "0";
                }
            }
        }
        return results;
    }
}





