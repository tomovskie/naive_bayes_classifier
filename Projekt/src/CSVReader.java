import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class CSVReader {

    public static List<CSVRecord> readFromCSV(String fileName) throws IOException {

        List<CSVRecord> recordsLearningSet = new ArrayList<>();

        final String SAMPLE_CSV_FILE_PATH = fileName;

        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        ) {
            for (CSVRecord csvRecord : csvParser) {
                recordsLearningSet.add(csvRecord);

            }
        }
        return  recordsLearningSet;
    }

    public static List<CSVRecord> readFromCSVTest(String fileName) throws IOException {

        List<CSVRecord> recordsTestSet = new ArrayList<>();

        final String SAMPLE_CSV_FILE_PATH = fileName;

        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        ) {
            for (CSVRecord csvRecord : csvParser) {
                recordsTestSet.add(csvRecord);

            }
        }
        return  recordsTestSet;
    }

}
